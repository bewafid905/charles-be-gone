# Charles-be-gone


## Instructions
 
### Twitterfix
Sur firefox, installez l'add-on [TwitterFix](https://addons.mozilla.org/en-US/firefox/addon/twitterfix/) (si une page twitter est déjà ouverte, recharger-la: Ctrl+F5).


### Importer ou exporter la blocklist
Allez dans vos paramètres en cliquant sur votre photo de profil en haut à droite, puis dans Comptes bloqués (Blocked accounts).
![csv blocklist import 1](export-csv-instructions-1.png)

Téléchargez le fichier contenant la liste des comptes à bloquer ici: [charles-be-gone.csv](https://framagit.org/charles-be-gone/charles-be-gone/-/raw/master/charles-be-gone.csv) (click droit et "Enregistrer sous...")

Puis cliquez sur Options avancées (Advanced options) et Importer un liste (Import a list) et importez le fichier téléchargé.
![csv blocklist import 2](export-csv-instructions-2.png)

Vous pouvez désinstaller l'add-on et utiliser le navigateur ou l'app de votre choix pour utiliser twitter après, ces étapes ne sont utiles que pour importer la liste.


## Alternative: blocktogether
Blocktogether est une app twitter qui permet de partager des blocklists.

Allez sur [le site blocktogether](https://blocktogether.org) et loggez-vous avec vos identifiants twitter, authorisez l'app et utilisez cette liste: https://blocktogether.org/show-blocks/iUMqmjdnlK4bIdz04N2m62oE6lXDdro45HdqvDjf 


Et voilà !
